use std::collections::HashSet;

#[derive(Clone)]
/// Columns must have a single type throughout
pub enum ColumnType {
    Float(Vec<f32>),
    Boolean(Vec<bool>),
    Category(Vec<usize>),
    Ordinal(Vec<i16>),
}

#[derive(Clone)]
pub struct ColumnData {
    pub data: ColumnType,
    pub data_len: usize,
}

impl ColumnData {
    pub fn len(&self) -> usize {
        self.data_len
    }
    pub fn is_empty(&self) -> bool {
        self.data_len == 0
    }
}

#[derive(Clone)]
/// Approaches to bisecting a Column
pub enum ColumnBoundary {
    /// Start & End of Section, Inclusive
    Float((f32, f32)),
    /// Can take (F,T) - False & True in Set, (F,F) - Only False, (T,T) - Only True.
    Bool(bool, bool),
    /// Values exist within the provided HashSet
    Category(HashSet<usize>),
    /// Values exist within the provided HashSet
    Ordinal(HashSet<i16>),
}
