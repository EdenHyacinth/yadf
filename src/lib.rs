extern crate num_traits;

pub mod column_data;
pub mod column_iterator;
pub mod column_view;

use crate::column_data::{ColumnData, ColumnType};

pub struct DataFrame {
    pub data: Vec<ColumnData>,
    rows: usize,
    columns: usize,
}

impl DataFrame {
    pub fn new(data: Vec<ColumnData>) -> DataFrame {
        let columns: usize = data.len();
        let rows = match &data[0].data {
            ColumnType::Float(column) => column.len(),
            ColumnType::Boolean(column) => column.len(),
            ColumnType::Ordinal(column) => column.len(),
            ColumnType::Category(column) => column.len(),
        };
        DataFrame {
            data,
            rows,
            columns,
        }
    }
    pub fn rows(&self) -> usize {
        self.rows
    }
    pub fn columns(&self) -> usize {
        self.columns
    }
}
