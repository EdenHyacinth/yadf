use crate::column_data::ColumnType;
use crate::column_view::ColumnView;

/*
    View Iterator is designed to ignore the penalty of having to do bounds checking on a
    non-contiguous segment, which is done in Rust's default iterator for contiguous segments.

    i.e
    >   vector_data.into_iter().fold(0.0 |a,b| a + b)
    experiences no bounds checks during iterations, as it is iterating over the entire set of data

    We can avoid this bounds check explicitly by declaring `get_unchecked`, but this is an unsafe
    function. We are making this 'safe' by checking the iterator cannot exceed the length of the
    data in the creation of Column View and only implementing the trait over ColumnView within the
    macro.
*/
macro_rules! impl_view_iterator {
    ($e:ty, $f:path) => {
        impl<'a> Iterator for ColumnView<'a, $e> {
            type Item = $e;
            fn next(&mut self) -> Option<Self::Item> {
                if self.index >= self.indices_len {
                    None
                } else {
                    self.index += 1_usize;
                    Some(unsafe {
                        match &self.data.data {
                            $f(column) => column.get_unchecked(self.indices[self.index - 1]).clone(),
                            _ => unreachable!()
                        }
                    })
                }
            }
        }
    }
}

impl_view_iterator!(f32, ColumnType::Float);
impl_view_iterator!(bool, ColumnType::Boolean);
impl_view_iterator!(usize, ColumnType::Category);
impl_view_iterator!(i16, ColumnType::Ordinal);

#[cfg(test)]
mod tests {
    use crate::column_data::{ColumnData, ColumnType};
    use crate::column_view::*;
    use crate::DataFrame;
    use std::marker::PhantomData;

    #[test]
    fn explicit_column_view_fold() {
        let c_data: ColumnData = ColumnData {
            data: ColumnType::Float(vec![0.0_f32, 1.0, 25.0, 3.0, 4.0, 15.5, 16.4, 7.0]),
            data_len: 7_usize,
        };
        //assert_eq view_column(c_data, vec![0, 1, 2, 3]).fold(0.0_f32, |a, b| { a + b }), 29.0_f32);
        let cv = ColumnView {
            data: &c_data,
            indices_len: 4_usize,
            indices: vec![0, 1, 2, 3],
            index: 0,
            phantom: PhantomData::<f32>,
        };
        assert_eq!(cv.fold(0.0_f32, |a, b| { a + b }), 29.0_f32);
    }

    #[test]
    fn column_view_from_column_data() {
        let c_data: ColumnData = ColumnData {
            data: ColumnType::Float(vec![0.0_f32, 1.0, 25.0, 3.0, 4.0, 15.5, 16.4, 7.0]),
            data_len: 8_usize,
        };
        let y: f32 = c_data.sum(vec![0, 1, 2, 3_usize]);
        assert_eq!(y, 29.0f32);
    }

    #[test]
    fn column_view_from_data_frame() {
        let c1_data: ColumnData = ColumnData {
            data: ColumnType::Float(vec![0.0_f32, 1.0, 25.0, 3.0, 4.0, 15.5, 16.4, 7.0]),
            data_len: 8_usize,
        };
        let c2_data: ColumnData = ColumnData {
            data: ColumnType::Ordinal(vec![0_i16, 2, 3, 2, 10, 15, 16, 7]),
            data_len: 8_usize,
        };
        let df: DataFrame = DataFrame::new(vec![c1_data, c2_data]);
        let x: f32 = df.data[0].sum(vec![0, 1, 2, 3_usize]);
        let y: i16 = df.data[1].sum(vec![0, 1, 2, 3_usize]);
        assert_eq!(x, 29_f32);
        assert_eq!(y, 7_i16);
    }
}
