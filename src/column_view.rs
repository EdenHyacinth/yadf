use std::marker::PhantomData;

use num_traits::{Bounded, Float, Num};

use crate::column_data::{ColumnData, ColumnType};
use std::cmp::Ordering;

#[derive(Clone)]
/// A View onto a column is a specifically non-mutable method of accessing the data underneath.
/// Views explicitly have indices to indicate which sections of data they intend to access, and a
/// phantom to indicate the type of data within the ColumnData
// TODO - Make Private after adjusting Tests
pub struct ColumnView<'a, T> {
    pub data: &'a ColumnData,
    pub indices_len: usize,
    pub indices: Vec<usize>,
    pub index: usize,
    pub phantom: PhantomData<T>,
}

impl<'a, T> ColumnView<'a, T> {
    pub fn len(&self) -> usize {
        self.indices_len
    }
    pub fn is_empty(&self) -> bool {
        self.indices_len == 0_usize
    }
}

pub trait ViewNonContiguousSlice<'a>: Sized {
    fn view(v: &'a ColumnData, indices: Vec<usize>) -> Option<Self>;
}

macro_rules! impl_column_view {
    ($e:ty, $f:path) => {
        impl<'a> ViewNonContiguousSlice<'a> for ColumnView<'a, $e> {
            fn view(v: &'a ColumnData, indices: Vec<usize>) -> Option<Self> {
                match &(*v).data {
                    $f(_) => {
                        Some(ColumnView::<$e> {
                            data: v,
                            indices_len: indices.len(),
                            indices,
                            index: 0,
                            phantom: PhantomData::<$e>
                        })
                    },
                    _ => None
                }
            }
        }
    }
}

impl_column_view!(f32, ColumnType::Float);
impl_column_view!(bool, ColumnType::Boolean);
impl_column_view!(usize, ColumnType::Category);
impl_column_view!(i16, ColumnType::Ordinal);

impl ColumnData {
    pub fn sum<T>(&self, indices: Vec<usize>) -> T
    where
        T: Num,
        for<'a> ColumnView<'a, T>: ViewNonContiguousSlice<'a> + Iterator<Item = T>,
    {
        Self::view_column::<ColumnView<'_, T>>(&self, indices).fold(T::zero(), |a, b| a + b)
    }

    pub fn accumulate<T>(&self, indices: Vec<usize>) -> Vec<T>
    where
        for<'a> ColumnView<'a, T>: ViewNonContiguousSlice<'a> + Iterator<Item = T>
    {
        Self::view_column::<ColumnView<'_, T>>(&self, indices).collect::<Vec<T>>()
    }

    pub fn min<T>(&self, indices: Vec<usize>) -> T
    where
        T: Num + Bounded + PartialOrd,
        for<'a> ColumnView<'a, T>: ViewNonContiguousSlice<'a> + Iterator<Item = T>,
    {
        Self::view_column::<ColumnView<'_, T>>(&self, indices).fold(T::max_value(), |acc, elem| {
            match acc.partial_cmp(&elem) {
                Some(Ordering::Less) => acc,
                Some(Ordering::Equal) => acc,
                Some(Ordering::Greater) => elem,
                None => acc,
            }
        })
    }

    pub fn max<T>(&self, indices: Vec<usize>) -> T
    where
        T: Num + Bounded + PartialOrd,
        for<'a> ColumnView<'a, T>: ViewNonContiguousSlice<'a> + Iterator<Item = T>,
    {
        Self::view_column::<ColumnView<'_, T>>(&self, indices).fold(T::min_value(), |acc, elem| {
            match acc.partial_cmp(&elem) {
                Some(Ordering::Less) => elem,
                Some(Ordering::Equal) => acc,
                Some(Ordering::Greater) => acc,
                None => acc,
            }
        })
    }

    pub fn standard_deviation<T>(&self, indices: Vec<usize>) -> T
    where
        T: Num + Copy + Float,
        for<'a> ColumnView<'a, T>: ViewNonContiguousSlice<'a> + Iterator<Item = T>,
    {
        let index_length: T = T::from(indices.len()).unwrap();
        let index_length_minus_one: T = T::from(indices.len() - 1).unwrap();
        let mean = self.sum(indices.clone()) / index_length;
        (Self::view_column::<ColumnView<'_, T>>(&self, indices).fold(T::zero(), |a, b| {
            let x = b - mean;
            a + x * x
        }) / index_length_minus_one)
            .sqrt()
    }

    pub fn view_column<'a, T: ViewNonContiguousSlice<'a>>(&'a self, indices: Vec<usize>) -> T {
        match T::view(self, indices) {
            Some(x) => x,
            None => panic!("Column has mixed typed between expected and found."),
        }
    }
}
