use criterion::black_box;
use criterion::Criterion;
use std::marker::PhantomData;
use yet_another_data_frame::column_data::{ColumnData, ColumnType};
use yet_another_data_frame::column_view::ColumnView;

extern crate criterion;
extern crate yet_another_data_frame;

#[derive(Clone)]
pub struct ExampleVec {
    pub data: Vec<f32>,
    pub indices_len: usize,
    pub indices: Vec<usize>,
    pub index: usize,
}

impl Iterator for ExampleVec {
    type Item = f32;
    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= self.indices_len {
            None
        } else {
            self.index += 1_usize;
            Some(unsafe {
                self.data
                    .get_unchecked(self.indices[self.index - 1])
                    .clone()
            })
        }
    }
}

fn column_view(c: &mut Criterion) {
    let data: ColumnData = ColumnData {
        data: ColumnType::Float(vec![0.0, 1.0, 2.0, 3.0]),
        data_len: 4_usize,
    };
    c.bench_function("ColumnView", move |b| {
        let cv = ColumnView::<f32> {
            data: &data,
            indices_len: 4,
            indices: vec![0_usize, 1, 2, 3],
            index: 0,
            phantom: PhantomData::<f32>,
        };
        b.iter(|| black_box(cv.clone().fold(0_f32, |a, b| a + b)))
    });
}

fn regular_view(c: &mut Criterion) {
    c.bench_function("RegularView", move |b| {
        let cv: ExampleVec = ExampleVec {
            data: vec![0.0_f32, 1.0, 2.0, 3.0],
            indices_len: 4_usize,
            indices: vec![0, 1, 2, 3],
            index: 0_usize,
        };
        b.iter(|| black_box(cv.clone().fold(0_f32, |a, b| a + b)))
    });
}

fn main() {
    let mut criterion = Criterion::default().configure_from_args().sample_size(35);
    column_view(&mut criterion);
    regular_view(&mut criterion);
    criterion.final_summary();
}
